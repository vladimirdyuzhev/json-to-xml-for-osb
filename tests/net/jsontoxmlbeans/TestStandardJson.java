package net.jsontoxmlbeans;

import org.apache.xmlbeans.XmlObject;

public class TestStandardJson extends Base {

	public void testEmpty() throws Exception {
		String orig = "   ";
		
		XmlObject xml = Convertor.toXML(orig);
		assertNull(xml);
	}	
	
	public void testEmptyObject() throws Exception {
		String orig = "{}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("1","count(/json/object)",s);
		assertXPath("0","count(/json/object/*)",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testEmptyObjectWithSpaces() throws Exception {
		String orig = "\n{ \r }\t";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("1","count(/json/object)",s);
		assertXPath("0","count(/json/object/*)",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals("{}",json);
	}
	
}

package net.jsontoxmlbeans;

import org.apache.xmlbeans.XmlObject;

public class TestBasics extends Base {

	public void testNull() throws Exception {
		assertNull(Convertor.toXML(null));
		assertNull(Convertor.toJSON(null));
	}

	public void testEmpty() throws Exception {
		assertNull(Convertor.toXML(""));
	}

	public void testEmptyObject() throws Exception {
		String orig = "{}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);

		assertXPath("1","count(/json/object)",s);		
		assertXPath("0","count(/json/object/*)",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_WithObjectValue() throws Exception {
		String orig = "{\"a\":{\"b z\":123}}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("123","/json/object/field[@name='a']/object/field[@name='b z']/number/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_UnderXmlBeans() throws Exception {
		String orig = "{\"a\":{\"b z\":123}}";
		XmlObject jsonUnderXml = XmlObject.Factory.parse("<foo:foo xmlns:foo='foo.foo'>"+orig+"</foo:foo>");
		
		XmlObject xml = Convertor.toXML(jsonUnderXml);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("123","/json/object/field[@name='a']/object/field[@name='b z']/number/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_EmptyUnderXmlBeans() throws Exception {
		XmlObject jsonUnderXml = XmlObject.Factory.parse("<foo:foo xmlns:foo='foo.foo'/>");
		
		XmlObject xml = Convertor.toXML(jsonUnderXml);
		assertNull(xml);
	}
	
	public void testBasicObject_WithSingleQuotes() throws Exception {
		String orig = "{'a':100}";
		
		try {
			Convertor.toXML(orig);
			fail("Invalid syntax");
		} catch( Exception ex ) {
		}
	}
	
	public void testBasicObject_WithNumberValue() throws Exception {
		String orig = "{\"a\":100}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("100","/json/object/field[@name='a']/number/@value",s);

		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
	
	public void testBasicObject_WithStringValue() throws Exception {
		String orig = "{\"a\":\"100\"}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("100","/json/object/field[@name='a']/string/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_WithTrueValue() throws Exception {
		String orig = "{\"a\":true}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("true","/json/object/field[@name='a']/boolean/@value",s);

		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_WithFalseValue() throws Exception {
		String orig = "{\"a\":false}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("false","/json/object/field[@name='a']/boolean/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}

	public void testBasicObject_WithNullValue() throws Exception {
		String orig = "{\"a\":null}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("1","count(/json/object/field[@name='a']/null)",s);
		assertXPath("0","count(/json/object/field[@name='a']/null/*)",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
	
//	public void testTopLevel_NullValue() throws Exception {
//		String orig = "null";
//		
//		XmlObject xml = Convertor.toXML(orig);
//		String s = xml.toString();
//		// assertSchema(s);
//		
//		assertXPath("1","count(/json/null)",s);
//		assertXPath("0","count(/json/null/*)",s);
//		
//		String json = Convertor.toJSON(xml);
//		assertEquals(orig,json);
//	}
	
	public void testBasicArray_WithNumberValues() throws Exception {
		String orig = "[1,2,3]";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("1","/json/array/number[1]/@value",s);
		assertXPath("2","/json/array/number[2]/@value",s);
		assertXPath("3","/json/array/number[3]/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
	
	public void testBasicArray_WithMixedValues() throws Exception {
		String orig = "[1,\"foo\",true,false,{\"val\":123},[5,6,7],null]";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("number","local-name(/json/array/*[1])",s);
		assertXPath("string","local-name(/json/array/*[2])",s);
		assertXPath("boolean","local-name(/json/array/*[3])",s);
		assertXPath("boolean","local-name(/json/array/*[4])",s);
		assertXPath("object","local-name(/json/array/*[5])",s);
		assertXPath("array","local-name(/json/array/*[6])",s);
		assertXPath("null","local-name(/json/array/*[7])",s);		
		
		assertXPath("1","/json/array/*[1]/@value",s);
		assertXPath("foo","/json/array/*[2]/@value",s);
		assertXPath("true","/json/array/*[3]/@value",s);
		assertXPath("false","/json/array/*[4]/@value",s);
		assertXPath("123","/json/array/*[5]/field[@name='val']/number/@value",s);
		assertXPath("5","/json/array/*[6]/number[1]/@value",s);
		assertXPath("6","/json/array/*[6]/number[2]/@value",s);
		assertXPath("7","/json/array/*[6]/number[3]/@value",s);
		assertXPath("1","count(/json/array/*[7])",s);
		assertXPath("0","count(/json/array/*[7]/*)",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
	
	public void testBasic_ArrayInObject_ObjectInArray() throws Exception {
		String orig = "{\"a\":{\"b\":[[2,3],{\"z\":[7,8]}]}}";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);

		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
	
	public void test165K() throws Exception {
		String orig = readFile("tests/165K.json");
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);

		String json = Convertor.toJSON(xml);
		// assertEquals(orig,json);
	}
	
	public void testNumbers() throws Exception {
		String orig = "[-1,0.111,1.23e+77,-6.14E-3,6436.1]";
		
		XmlObject xml = Convertor.toXML(orig);
		String s = xml.toString();
		assertSchema(s);
		
		assertXPath("-1","/json/array/number[1]/@value",s);
		assertXPath("0.111","/json/array/number[2]/@value",s);
		assertXPath("1.23e+77","/json/array/number[3]/@value",s);
		assertXPath("-6.14E-3","/json/array/number[4]/@value",s);
		assertXPath("6436.1","/json/array/number[5]/@value",s);
		
		String json = Convertor.toJSON(xml);
		assertEquals(orig,json);
	}
}

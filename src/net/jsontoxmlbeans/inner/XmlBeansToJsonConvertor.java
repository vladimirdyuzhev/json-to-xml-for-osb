package net.jsontoxmlbeans.inner;

import static argo.jdom.JsonNodeBuilders.aFalseBuilder;
import static argo.jdom.JsonNodeBuilders.aNullBuilder;
import static argo.jdom.JsonNodeBuilders.aNumberBuilder;
import static argo.jdom.JsonNodeBuilders.aStringBuilder;
import static argo.jdom.JsonNodeBuilders.aTrueBuilder;
import static argo.jdom.JsonNodeBuilders.anArrayBuilder;
import static argo.jdom.JsonNodeBuilders.anObjectBuilder;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import argo.format.CompactJsonFormatter;
import argo.format.JsonFormatter;
import argo.jdom.JsonArrayNodeBuilder;
import argo.jdom.JsonNodeBuilder;
import argo.jdom.JsonObjectNodeBuilder;
import argo.jdom.JsonRootNode;

@SuppressWarnings("rawtypes")
public class XmlBeansToJsonConvertor {
	private static final JsonFormatter JSON_FORMATTER = new CompactJsonFormatter();
	
	private JsonNodeBuilder builder;
	
	public XmlBeansToJsonConvertor(XmlObject xml) throws XmlException {
		if( xml == null ) return;

		XmlCursor cur = xml.newCursor();
		cur.toStartDoc();
		boolean got = cur.toChild(Utils.qname("json"));
		if( got ) {
			boolean child = cur.toChild(0);
			if( !child ) return;
			
			builder = walkChild(cur);
		}
	}

	public String getJSON() {
		JsonRootNode json = buildRootNode();
		if( json == null ) return null;
		return JSON_FORMATTER.format(json);
	}
	
	private JsonRootNode buildRootNode() {
		if( builder == null ) return null;
		return (JsonRootNode) builder.build();
	}

	private JsonNodeBuilder walkChild(XmlCursor cur) throws XmlException {
		String name = cur.getName().getLocalPart();
		
		if( "object".equals(name) ) return walkObject(cur);
		else if( "array".equals(name) ) return walkArray(cur);
		else if( "number".equals(name) ) return walkNumber(cur);
		else if( "string".equals(name) ) return walkString(cur);
		else if( "boolean".equals(name) ) return walkBoolean(cur);
		else if( "null".equals(name) ) return walkNull(cur);
		else throw new RuntimeException("unknown child name: "+name);
	}

	private JsonNodeBuilder walkNull(XmlCursor cur) {
		return aNullBuilder();
	}

	private JsonNodeBuilder walkBoolean(XmlCursor cur) throws XmlException {
		String val = cur.getAttributeText(new QName("value"));
		if( "true".equals(val) ) return aTrueBuilder();
		else if( "false".equals(val) ) return aFalseBuilder();
		else throw new XmlException("unknown value for boolean: "+val+", allowed true|false");
	}

	private JsonNodeBuilder walkNumber(XmlCursor cur) {
		String val = cur.getAttributeText(new QName("value"));
		return aNumberBuilder(val);
	}

	private JsonNodeBuilder walkString(XmlCursor cur) {
		String val = cur.getAttributeText(new QName("value"));
		return aStringBuilder(val);
	}

	private void walkField(XmlCursor cur,JsonObjectNodeBuilder ob) throws XmlException {
		try {
			cur = cur.newCursor();
			
			String name = cur.getAttributeText(new QName("name"));
			
			boolean child = cur.toChild(0);
			if( !child ) throw new XmlException("field element must have a nested value element, e.g. object, array, string, number, boolean or null"); 
			
			JsonNodeBuilder value = walkChild(cur);
			ob.withField(name, value);
		} finally {
			cur.dispose();
		}
	}

	private JsonArrayNodeBuilder walkArray(XmlCursor cur) throws XmlException {
		try {
			cur = cur.newCursor();
			
			JsonArrayNodeBuilder nb = anArrayBuilder();

			boolean child = cur.toChild(0);
			while(child) {
				JsonNodeBuilder cnb = walkChild(cur);
				nb.withElement(cnb);
				
				child = cur.toNextSibling();
			}
			
			return nb;
		} finally {
			cur.dispose();
		}
	}

	private JsonNodeBuilder walkObject(XmlCursor cur) throws XmlException {
		try {
			cur = cur.newCursor();
			
			JsonObjectNodeBuilder ob = anObjectBuilder();
			
			boolean child = cur.toChild(0);
			while(child) {
				walkField(cur,ob);
				
				child = cur.toNextSibling();
			}
			
			return ob;
		} finally {
			cur.dispose();
		}
	}
}

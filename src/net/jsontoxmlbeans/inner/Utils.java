package net.jsontoxmlbeans.inner;

import javax.xml.namespace.QName;

public class Utils {
	static final String OJTX_NS = "http://json.to.xml.for.osb";

	static QName qname(String name) {
		return new QName(OJTX_NS,name);
	}
}
